INSERT INTO authority (id, role)
VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_ADMIN');

INSERT INTO user(id, email, name, password, username, authority_id)
VALUES
(1, 'teji.catia@gmail.com','Tejinder Singh', 'password', 'tejinder', 2),
(2, 'abhinay@alphaseq.com','Abhinay Jiwankar', 'password', 'abhinay', 1),
(3, 'sabia@alphaseq.com','Sabia Virdi', 'password', 'sabia', 1);


INSERT INTO register_form_data(id, name, email, mobile, addr1, addr2, city, country, dateofp, deviceid, pin, state)
VALUES
(1, 'Jeremy K. Monk', 'JeremyKMonk@dayrep.com', '530-823-4093', '2259 Byers Lane', 'Auburn, CA', 'Auburn', 'USA', '2018-03-11 09:56:14','123456','95603','CA'),
(2, 'William C. McLean', 'WilliamCMcLean@armyspy.com', '252-474-8744', '4544 Green Acres Road', 'Greenville, NC', 'Greenville', 'USA', '2018-04-10 11:31:41','111111','27834','NC');

INSERT INTO devices (id, deviceid, devicekey, status, dateof_activation, customerid_id) 
VALUES 
(1, '123456', '654321', 'active', '2018-03-11 09:56:14', 1),
(2, '111111', '999999', 'active', '2018-04-10 11:31:41', 2),
(3, '222222', '888888', 'User Deleted', '2018-02-20 05:25:10', NULL),
(4, '333333', '777777', 'User Deleted', '2017-11-25 11:30:57', NULL),
(5, '444444', '666666', 'inactive', NULL, NULL),
(6, '555555', '555555', 'inactive', NULL, NULL),
(7, '666666', '444444', 'inactive', NULL, NULL),
(8, '777777', '333333', 'inactive', NULL, NULL),
(9, '888888', '222222', 'inactive', NULL, NULL),
(10, '999999', '111111', 'inactive', NULL, NULL);