function connect() {
	var socket = new SockJS('/chat-messaging');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
//		console.log("connected: " + frame);
		stompClient.subscribe('/chat/messages', function(response) {
			var data = JSON.parse(response.body);
//			console.log("data: " + JSON.stringify(data));
//			console.log("data.from: " + data.from);
			if(data.groupid==$("#groupid").val()){
				if (data.from==$("#message_sender").val()) {
					draw("right", data.message, data.from);
				}
				else{
					draw("left", data.message, data.from);
				}
		}
				 var elem = document.getElementById('msg');
				 elem.scrollTop = elem.scrollHeight;
				 
		});
	});
}

function draw(side, text, from) {
//	console.log("drawing...");
    var $message;

	
//	console.log("drawing..."+ JSON.stringify($message));
    $message = $($('.message_template').clone().html());
    $message.addClass(side).find('.text').html(text);
    $message.addClass(side).find('.from').html(from);
    $('.messages').append($message);
    return setTimeout(function () {
        return $message.addClass('appeared');
    }, 0);
   
	
}
function disconnect(){
	stompClient.disconnect();
}
function sendMessage(){
	
	var date = new Date(); 
	var timestamp = date.getTime();
	
	if($("#message_input_value").val()){
	stompClient.send("/app/message", {}, JSON.stringify({'message': $("#message_input_value").val(),
		'from': $("#message_sender").val(), 
		'to': $("#message_rec").val(),
		"timestamp": timestamp,
		'groupid': $("#groupid").val()}));
	$("#message_input_value").val("");
	var headerName = "${_csrf.headerName}";
	var token = "${_csrf.token}";
//	console.log("csrfheadname..."+headerName);
//	console.log("csrftoken..."+token);
	}
	
	

}
