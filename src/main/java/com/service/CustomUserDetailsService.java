package com.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.model.User;
import com.repository.UserRepository;
import com.util.UserNotFoundException;

@Service
@Qualifier("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	public UserDetails loadUserByUsername(String name) throws UserNotFoundException {
		User domainUser = userRepository.findByUsername(name);
		if (domainUser == null) {
			throw new UserNotFoundException();
		}
		String role = domainUser.getAuthority().getRole();
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(role));

		CustomUserDetail customUserDetail = new CustomUserDetail();
		customUserDetail.setUser(domainUser);
		customUserDetail.setAuthorities(authorities);
		return customUserDetail;

	}

}
