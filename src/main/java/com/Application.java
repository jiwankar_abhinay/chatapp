package com;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.model.User;
import com.repository.UserRepository;
import com.security.SecurityConfig;



@ServletComponentScan
@SpringBootApplication
@EnableTransactionManagement
public class Application implements CommandLineRunner{
	@Autowired
	UserRepository users;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	
	@Override
	public void run(String... strings)
	{
		Iterator<User> it = users.findAll().iterator();

		while(it.hasNext()){
			User u1 = it.next();
			String pass = u1.getPassword();
			String encodedPass = SecurityConfig.encoder.encode(pass);
			String prefix = pass.substring(0,4);
			if("$2a$".equals(prefix) || "$2b$".equals(prefix) || "$2y$".equals(prefix))
			{
				continue;
			}
			u1.setPassword(encodedPass);
			users.save(u1);
		}
	}

}
