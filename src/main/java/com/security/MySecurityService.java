package com.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.model.User;
import com.repository.UserRepository;
import com.service.CustomUserDetail;

@Component("mySecurityService")
public class MySecurityService {

	@Autowired
	UserRepository users;
	
	public boolean canEditHotel(long hotel_id, String s){
		return false;
	}
	
	public boolean canEditUser(long user_id, CustomUserDetail user){
		User userTmp = users.findOne(user_id);
		return userTmp != null && user.getUser() != null && user.getUser().getId() == userTmp.getId();
	}
	
}
