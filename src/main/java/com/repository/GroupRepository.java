package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Group;
import com.model.User;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {
	@Query("FROM Group G WHERE G.groupAdminId = :groupAdminId")
	List<Group> findAllByAdmin(@Param("groupAdminId") Long groupAdminId);
	
	Group findGroupByGroupName(String groupName);
}

