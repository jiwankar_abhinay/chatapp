package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.MessageBucket;

@Repository
public interface MessageBucketRepository extends CrudRepository<MessageBucket, Long> {
	
	@Query("FROM MessageBucket M WHERE (M.msgTo = :to AND M.msgFrom = :from) OR (M.msgTo = :from AND M.msgFrom = :to) ")
	List<MessageBucket> findAllToFrom(@Param("to") String to, @Param("from") String from);
	
	@Query("FROM MessageBucket M WHERE M.groupid = :groupid")
	List<MessageBucket> findAllGroupId(@Param("groupid") String groupid);
}

