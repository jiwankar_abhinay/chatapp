package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.model.Devices;


public interface DevicesRepository extends CrudRepository<Devices, Long> {
	
	Devices findByDeviceid(String deviceid);
}

