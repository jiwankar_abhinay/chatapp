package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.model.RegisterFormData;


public interface RegisterFormDataRepository extends CrudRepository<RegisterFormData, Long> {

	RegisterFormData findByDeviceid(String deviceid);
}

