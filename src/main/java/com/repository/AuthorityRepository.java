package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.model.Authority;


public interface AuthorityRepository extends CrudRepository<Authority, Long> {
	
	Authority findByRole(String role);
}

