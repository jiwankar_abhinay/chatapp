package com.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MessageBucket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6347814153068931218L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private Date date;
	 String msgTo;
	 String msgFrom;
	 @Column(length=3200)
		private String text;
		private String groupid;
	private long timest;
		
	public MessageBucket() {}
	
	



	@Override
	public String toString() {
		return "MessageBucket [id=" + id + ", date=" + date + ", msgTo=" + msgTo + ", msgFrom=" + msgFrom + ", text="
				+ text + ", groupid=" + groupid + ", timest=" + timest + "]";
	}





	public MessageBucket(Date date, String msgTo, String msgFrom, String text, String groupid, long timest) {
		super();
		this.date = date;
		this.msgTo = msgTo;
		this.msgFrom = msgFrom;
		this.text = text;
		this.groupid = groupid;
		this.timest = timest;
	}





	public String getGroupid() {
		return groupid;
	}





	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}





	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getTimest() {
		return timest;
	}









	public void setTimest(long timest) {
		this.timest = timest;
	}









	public void setDate(Date date) {
		this.date = date;
	}




	public String getMsgTo() {
		return msgTo;
	}




	public void setMsgTo(String msgTo) {
		this.msgTo = msgTo;
	}




	public String getMsgFrom() {
		return msgFrom;
	}




	public void setMsgFrom(String msgFrom) {
		this.msgFrom = msgFrom;
	}




	public void setId(long id) {
		this.id = id;
	}
	
}
