package com.model;

public class Message{
	
	private String to;
	private String from;
	private String message;
	private long timestamp;
	private String groupid;
	
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	
	
	@Override
	public String toString() {
		return "Message [to=" + to + ", from=" + from + ", message=" + message + ", timestamp=" + timestamp
				+ ", groupid=" + groupid + "]";
	}
	public Message(String from, String message, long timestamp, String groupid) {
		super();
		this.from = from;
		this.message = message;
		this.timestamp = timestamp;
		this.groupid = groupid;
	}
	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}