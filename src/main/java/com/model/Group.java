package com.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="GroupTable")
public class Group{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
//	@OneToOne

	@JoinColumn(name = "groupAdmin")
	@ManyToOne(targetEntity=User.class)
	private User groupAdmin;
	
	@Column(name = "groupAdminId")
	private Long groupAdminId;
	
	@ManyToMany
	private List<User> members;
	
	@Column(unique=true)
	private String groupName;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public User getGroupAdmin() {
		return groupAdmin;
	}
	public void setGroupAdmin(User groupAdmin) {
		this.groupAdmin = groupAdmin;
	}
	public List<User> getMembers() {
		return members;
	}
	public void setMembers(List<User> members) {
		this.members = members;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Group() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Group [id=" + id + ", groupAdmin=" + groupAdmin + ", members=" + members + ", groupName=" + groupName
				+ "]";
	}
	public long getGroupAdminId() {
		return groupAdminId;
	}
	public void setGroupAdminId(Long groupAdminId) {
		this.groupAdminId = groupAdminId;
	}
	
	
		
}