package com.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

@Entity
public class Devices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8035405300405264483L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Column(unique=true)
	private String deviceid;
	private String devicekey;
	private String status;
	
	@OneToOne
	private RegisterFormData customerid;
	
	private Date dateofActivation;
	
	
	@PrePersist
	public void prePersist() {
	    if(status == null)
	    	status = "inactive";
	}
	
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public String getDevicekey() {
		return devicekey;
	}
	public void setDevicekey(String devicekey) {
		this.devicekey = devicekey;
	}
	public String getStatus() {
		return (this.status == null) ? "inactive" : status;
//		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public RegisterFormData getCustomerid() {
		return customerid;
	}
	public void setCustomerid(RegisterFormData customerid) {
		this.customerid = customerid;
	}
	public Date getDateofActivation() {
		return dateofActivation;
	}
	public void setDateofActivation(Date dateofActivation) {
		this.dateofActivation = dateofActivation;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerid == null) ? 0 : customerid.hashCode());
		result = prime * result + ((dateofActivation == null) ? 0 : dateofActivation.hashCode());
		result = prime * result + ((deviceid == null) ? 0 : deviceid.hashCode());
		result = prime * result + ((devicekey == null) ? 0 : devicekey.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Devices other = (Devices) obj;
		if (customerid == null) {
			if (other.customerid != null)
				return false;
		} else if (!customerid.equals(other.customerid))
			return false;
		if (dateofActivation == null) {
			if (other.dateofActivation != null)
				return false;
		} else if (!dateofActivation.equals(other.dateofActivation))
			return false;
		if (deviceid == null) {
			if (other.deviceid != null)
				return false;
		} else if (!deviceid.equals(other.deviceid))
			return false;
		if (devicekey == null) {
			if (other.devicekey != null)
				return false;
		} else if (!devicekey.equals(other.devicekey))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Devices [id=" + id + ", deviceid=" + deviceid + ", devicekey=" + devicekey + ", status=" + status
				+ ", customerid=" + customerid + ", dateofActivation=" + dateofActivation + "]";
	}
	public Devices(String deviceid, String devicekey, String status, RegisterFormData customerid,
			Date dateofActivation) {
		super();
		this.deviceid = deviceid;
		this.devicekey = devicekey;
		this.status = status;
		this.customerid = customerid;
		this.dateofActivation = dateofActivation;
	}
	public Devices() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
