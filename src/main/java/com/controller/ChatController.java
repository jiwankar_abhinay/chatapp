package com.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.model.Message;
import com.model.MessageBucket;
import com.repository.MessageBucketRepository;


@Controller
public class ChatController {

	@Autowired
	MessageBucketRepository messages;
	
	@MessageMapping("/message")
	@SendTo("/chat/messages")
	public Message getMessages(Message message) {
		MessageBucket mb = new MessageBucket(new Date(), message.getTo(), message.getFrom(), message.getMessage(), message.getGroupid(), message.getTimestamp());
		messages.save(mb);
		System.out.println(message);
		return message;
	}
}