package com.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.model.Authority;
import com.model.Devices;
import com.model.Group;
import com.model.MessageBucket;
import com.model.RegisterFormData;
import com.model.User;
import com.repository.AuthorityRepository;
import com.repository.DevicesRepository;
import com.repository.GroupRepository;
import com.repository.MessageBucketRepository;
import com.repository.RegisterFormDataRepository;
import com.repository.UserRepository;
import com.security.AllowedForAdmin;
import com.security.AllowedForManageUser;
import com.security.SecurityConfig;
import com.service.CustomUserDetail;
import com.util.DeviceNotFoundException;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private JavaMailSender sender;

	@Autowired
	DevicesRepository devicesRepository;

	@Autowired
	UserRepository users;

	@Autowired
	GroupRepository groupRepository;

	@Autowired
	RegisterFormDataRepository formDataRepository;

	@Autowired
	AuthorityRepository authorities;
	@Autowired
	MessageBucketRepository messages;

	// GET /users - the list of users
	@AllowedForAdmin
	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		return "redirect:/user/me";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerDevice(@ModelAttribute RegisterFormData registerFormData, Model model) {
		try {
			User user = new User();
			String deviceid = registerFormData.getDeviceid();
			Devices device = devicesRepository.findByDeviceid(deviceid);
			System.out.println(device);
			if (device.getStatus().equalsIgnoreCase("Active")) {
				throw new Exception("Device Already registered");
			} else {
				device.setCustomerid(registerFormData);
				device.setDateofActivation(new Date());
				device.setStatus("Active");

				model.addAttribute("message", "Device is successfully activated");
				model.addAttribute("registerFormData", new RegisterFormData());
				try {
					// sendEmail(registerFormData);
					user.setAuthority(authorities.findOne((long) 1));
					user.setEmail(registerFormData.getEmail());
					user.setName(registerFormData.getName());
					user.setPassword(generateRandomPassword());
					user.setPhone(registerFormData.getMobile());
					user.setUsername(registerFormData.getEmail().split("@")[0]);
					sendTemplateMessage(registerFormData, user);
					formDataRepository.save(registerFormData);
					devicesRepository.save(device);
					user.setPassword(SecurityConfig.encoder.encode(user.getPassword()));
					users.save(user);
				} catch (Exception e) {
					model.addAttribute("registerFormData", registerFormData);
					model.addAttribute("message", "Email Server Error");
					e.printStackTrace();
					return "users/create";
				}
				return "users/create";
			}
		} catch (NullPointerException e) {
			model.addAttribute("registerFormData", registerFormData);
			model.addAttribute("message", "Invalid device ID");
			return "users/create";
		} catch (Exception e) {
			model.addAttribute("registerFormData", registerFormData);
			model.addAttribute("message", "Device is Already Registered / Activated");
			return "users/create";
		}
	}

	public String generateRandomPassword() {
		int length = 6;
		boolean useLetters = true;
		boolean useNumbers = true;
		return RandomStringUtils.random(length, useLetters, useNumbers);
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String regidterGet(Model model) {
		return "redirect:/user/new";
	}

	// GET /users/new - the form to fill the data for a new user
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newHotel(Model model) {
		model.addAttribute("registerFormData", new RegisterFormData());
		model.addAttribute("message", "");
		model.addAttribute("url", "/user/register");
		return "users/create";
	}

	// GET /login
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		return "users/login";
	}

	/*
	 * // GET /users/{id} - the user with identifier {id}
	 * 
	 * @RequestMapping(value = "{id}", method = RequestMethod.GET)
	 * 
	 * @AllowedForManageUser public String show(@PathVariable("id") long id, Model
	 * model) { RegisterFormData rform = formDataRepository.findOne(id); Devices
	 * device = devicesRepository.findByDeviceid(rform.getDeviceid()); // User user
	 * = users.findOne(id); if (rform == null || device == null) { throw new
	 * DeviceNotFoundException(); }
	 * 
	 * model.addAttribute("user", rform); model.addAttribute("device", device);
	 * return "users/show"; }
	 */

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String showActiveProfile(Model model, CsrfToken token, HttpServletRequest request,
			HttpServletResponse response) {

		Iterable<User> userlist = new ArrayList<>();
		userlist = users.findAll();
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User user = users.findOne(myUser.getUser().getId());
		if (user.getPhone() == null)
			user.setPhone("Not-Available");
		model.addAttribute("user", user);

		return "users/home";
	}

	@RequestMapping(value = "/home/{username}", method = RequestMethod.GET)
	public String showPrivateChat(Model model, CsrfToken token, HttpServletRequest request,
			HttpServletResponse response, @PathVariable("username") String username) {
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User currentUser = users.findOne(myUser.getUser().getId());
		model.addAttribute("user", currentUser);

		List<User> userlist = new ArrayList<>();
		userlist = (List<User>) users.findAll();
		userlist.remove(currentUser);
		model.addAttribute("user", currentUser);
		model.addAttribute("from", currentUser.getUsername());
		model.addAttribute("userlist", userlist);

		User toUser = users.findByUsername(username);
		List<MessageBucket> msgList = messages.findAllToFrom(username, currentUser.getUsername());
		// Iterator<MessageBucket> itr = msgList.iterator();
		// while(itr.hasNext()) System.out.println("---->"+itr.next().toString());
//		List<Group> grouplist = groupRepository.findAllByAdmin(currentUser.getId());

		List<Group> grouplistAsUser = new ArrayList<>();

		Iterable<Group> groupsList = groupRepository.findAll();
		Iterator<Group> itr = groupsList.iterator();
		while (itr.hasNext()) {
			Group grp = itr.next();
			Iterable<User> membrs = grp.getMembers();
			Iterator<User> it = membrs.iterator();
			while (it.hasNext()) {
				if (it.next().equals(currentUser)) {
					grouplistAsUser.add(grp);
					break;
				}
			}

		}

		model.addAttribute("msgList", msgList);
		model.addAttribute("touser", "Private: " + toUser.getName());
		model.addAttribute("to", toUser.getUsername());
		model.addAttribute("grouplist", grouplistAsUser);
		model.addAttribute("groupid", "0");
		return "users/chat";
	}

	@RequestMapping(value = "/group/{groupname}", method = RequestMethod.GET)
	public String showGroupChat(Model model, CsrfToken token, HttpServletRequest request, HttpServletResponse response,
			@PathVariable("groupname") String groupname) {
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User currentUser = users.findOne(myUser.getUser().getId());
		model.addAttribute("user", currentUser);

		List<User> userlist = new ArrayList<>();
		userlist = (List<User>) users.findAll();
		userlist.remove(currentUser);
		model.addAttribute("user", currentUser);
		model.addAttribute("from", currentUser.getUsername());
		model.addAttribute("userlist", userlist);

		Group group = groupRepository.findGroupByGroupName(groupname);
		List<MessageBucket> msgList = messages.findAllGroupId(new Long(group.getId()).toString());
		// Iterator<MessageBucket> itr = msgList.iterator();
		// while(itr.hasNext()) System.out.println("---->"+itr.next().toString());
		List<Group> grouplistAsAdmin = groupRepository.findAllByAdmin(currentUser.getId());
		List<Group> grouplistAsUser = new ArrayList<>();

		Iterable<Group> groupsList = groupRepository.findAll();
		Iterator<Group> itr = groupsList.iterator();
		while (itr.hasNext()) {
			Group grp = itr.next();
			Iterable<User> membrs = grp.getMembers();
			Iterator<User> it = membrs.iterator();
			while (it.hasNext()) {
				if (it.next().equals(currentUser)) {
					grouplistAsUser.add(grp);
					break;
				}
			}

		}

		model.addAttribute("groupid", group.getId());

		model.addAttribute("msgList", msgList);
		model.addAttribute("touser", "Group: " + groupname);
		model.addAttribute("to", group.getGroupAdmin().getUsername());
		model.addAttribute("grouplist", grouplistAsUser);// as subscriber

		return "users/groupchat";
	}

	@RequestMapping(value = "/chat", method = RequestMethod.GET)
	public String showChatAplication(Model model, CsrfToken token, HttpServletRequest request,
			HttpServletResponse response) {
		Iterable<User> userlist = new ArrayList<>();
		userlist = users.findAll();
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User user = users.findOne(myUser.getUser().getId());
		List<Group> grouplist = groupRepository.findAllByAdmin(user.getId());

		model.addAttribute("user", user);
		model.addAttribute("from", user.getUsername());
		model.addAttribute("userlist", userlist);
		model.addAttribute("grouplist", grouplist);

		return "users/chat";
	}

	@RequestMapping(value = "/group/create", method = RequestMethod.GET)
	public String createGroup(Model model, CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
		Iterable<User> userlist = new ArrayList<>();
		userlist = users.findAll();
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User user = users.findOne(myUser.getUser().getId());

		ArrayList<User> userls = (ArrayList<User>) userlist;
		userls.remove(user);

		model.addAttribute("user", user);
		model.addAttribute("from", user.getUsername());
		model.addAttribute("userlist", userls);

		return "users/createGroup";
	}

	@RequestMapping(value = "/group/create", method = RequestMethod.POST)
	public String createGroupFormAction(Model model, @RequestParam("groupName") String groupName,
			@RequestParam("members") List<String> members, CsrfToken token, HttpServletRequest request,
			HttpServletResponse response) {

		System.err.println(groupName + "\t\t\t" + members);

		Iterable<User> userlist = new ArrayList<>();
		userlist = users.findAll();
		response.setHeader(token.getHeaderName(), token.getToken());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		Group group = new Group();
		User user = users.findOne(myUser.getUser().getId());
		List<User> userList = new ArrayList<>();
		userList.add(user);
		Iterator<String> itr = members.iterator();
		while (itr.hasNext()) {
			User userToAdd = users.findOne(new Long(itr.next()));
			userList.add(userToAdd);
		}
		group.setGroupAdminId(user.getId());
		group.setGroupAdmin(user);
		group.setMembers(userList);
		group.setGroupName(groupName);
		groupRepository.save(group);
		model.addAttribute("user", user);
		model.addAttribute("from", user.getUsername());
		model.addAttribute("userlist", userlist);

		return "redirect:/user/group/"+groupName;
	}

	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public String showProfile(Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CustomUserDetail myUser = (CustomUserDetail) auth.getPrincipal();
		User user = users.findOne(myUser.getUser().getId());
		model.addAttribute("user", user);
		model.addAttribute("from", user.getUsername());
		if (user.getAuthority().equals("ROLE_ADMIN")) {
			return "redirect:/admin";
		} else {
			return "redirect:/user/home";
		}
	}

	/*
	 * @RequestMapping(value = "{id}/remove", method = RequestMethod.GET)
	 * 
	 * @AllowedForManageUser public String remove(@PathVariable("id") long id, Model
	 * model) {
	 * 
	 * RegisterFormData rform = formDataRepository.findOne(id); Devices device =
	 * devicesRepository.findByDeviceid(rform.getDeviceid());
	 * device.setCustomerid(null); device.setStatus("User Deleted");
	 * devicesRepository.save(device); formDataRepository.delete(rform); return
	 * "redirect:/admin"; }
	 */

	/*
	 * @RequestMapping(value = "{id}/edit", method = RequestMethod.GET) public
	 * String edit(@PathVariable("id") long id, Model model) { RegisterFormData
	 * rform = formDataRepository.findOne(id); Devices device =
	 * devicesRepository.findByDeviceid(rform.getDeviceid()); if (rform == null ||
	 * device == null) { throw new DeviceNotFoundException(); }
	 * model.addAttribute("registerFormData", rform); model.addAttribute("url",
	 * "/user/" + id); return "users/edit"; }
	 */
	/*
	 * // POST /users/{id} - edit a user
	 * 
	 * @RequestMapping(value = "/{id}", method = RequestMethod.POST) public String
	 * edit(@PathVariable("id") long id, @ModelAttribute RegisterFormData rform,
	 * Model model) { formDataRepository.save(rform); return "redirect:/user/" + id;
	 * }
	 */

	@SuppressWarnings("unused")
	private void sendEmail(RegisterFormData registerFormData) throws Exception {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo(registerFormData.getEmail());
		helper.setSubject("Device Registration Successful");
		helper.setText("This is a test mail to show your registration details\n\n\n"
				+ registerFormData.toString().replaceAll(", ", "\n"));
		sender.send(message);
	}

	public void sendTemplateMessage(RegisterFormData registerFormData, User user)
			throws MessagingException, IOException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());
		Context ctx = new Context();
		ctx.setVariable("name", registerFormData.getName());

		ctx.setVariable("username", user.getUsername());
		ctx.setVariable("password", user.getPassword());

		String html = templateEngine.process("emailTemplate", ctx);
		helper.setTo(registerFormData.getEmail());
		helper.setSubject("Device Registration Successful");
		helper.setText(html, true);

		// helper.setText("This is a test mail to show your registration
		// details\n\n\n"+registerFormData.toString().replaceAll(", ", "\n"));
		sender.send(message);

	}

}
