package com.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.repository.DevicesRepository;
import com.repository.RegisterFormDataRepository;
import com.security.AllowedForAdmin;
import com.service.CustomUserDetail;

@Controller
public class ApplicationController {
	
//	@Autowired
//	UserRepository users;
//	
	@Autowired
	DevicesRepository devices;
	
	@Autowired
	RegisterFormDataRepository regforms;

	@RequestMapping(value="/")
	public String root(Model model, HttpServletRequest request) {
		
		return "landing-page";
	}
	
//	@RequestMapping(value="/email")
//	public String emailTemplate(Model model, HttpServletRequest request) {
//		model.addAttribute("name", "Abhinay");
//		return "emailTemplate";
//	}
	
	@RequestMapping(value="/signedin")
	public String signedIn(Model model, Authentication authentication) {
		CustomUserDetail principal = (authentication != null) ? (CustomUserDetail) authentication.getPrincipal() : null;
		if (principal != null) {
			String a = ((SimpleGrantedAuthority) principal.getAuthorities().iterator().next()).getAuthority();
			if (a.equals(("ROLE_ADMIN")))
				return "redirect:/admin";
			else if (a.equals(("ROLE_USER")))
				return "redirect:/user/home";
//				return "users/home";
		}
		return "/"; // fallback
	}
	
	/*@RequestMapping(value="/admin")
	@AllowedForAdmin
	public String manageUsers(Model model)
	{
		model.addAttribute("users", regforms.findAll());

		model.addAttribute("devices", devices.findAll());
		
		return "admin-dashboard";
	}*/
}
