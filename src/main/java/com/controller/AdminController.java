package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.model.Devices;
import com.model.RegisterFormData;
import com.repository.AuthorityRepository;
import com.repository.DevicesRepository;
import com.repository.MessageBucketRepository;
import com.repository.RegisterFormDataRepository;
import com.repository.UserRepository;
import com.util.DeviceNotFoundException;


@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	@Autowired
	DevicesRepository devices;
	
	@Autowired
	RegisterFormDataRepository regforms;

	@Autowired
	DevicesRepository devicesRepository;
	
	@Autowired
	UserRepository users;
	
	@Autowired
	RegisterFormDataRepository formDataRepository;

	@Autowired
	AuthorityRepository authorities;
	
	@Autowired
	MessageBucketRepository messages;

	@RequestMapping(method=RequestMethod.GET)
	public String manageUsers(Model model){
		model.addAttribute("users", regforms.findAll());
		model.addAttribute("devices", devices.findAll());
		return "admin-dashboard";
	}

	// GET /users/{id} - the user with identifier {id}
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") long id, Model model) {
		RegisterFormData rform = formDataRepository.findOne(id);
		Devices device = devicesRepository.findByDeviceid(rform.getDeviceid());
		// User user = users.findOne(id);
		if (rform == null || device == null) {
			throw new DeviceNotFoundException();
		}

		model.addAttribute("user", rform);
		model.addAttribute("device", device);
		return "users/show";
	}

	@RequestMapping(value = "{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable("id") long id, Model model) {
		
		RegisterFormData rform = formDataRepository.findOne(id);
		Devices device = devicesRepository.findByDeviceid(rform.getDeviceid());
		device.setCustomerid(null);
		device.setStatus("User Deleted");
		devicesRepository.save(device);
		formDataRepository.delete(rform);
		return "redirect:/admin";
	}

	@RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, Model model) {
		RegisterFormData rform = formDataRepository.findOne(id);
		Devices device = devicesRepository.findByDeviceid(rform.getDeviceid());
		if (rform == null || device == null) {
			throw new DeviceNotFoundException();
		}
		model.addAttribute("registerFormData", rform);
		model.addAttribute("url", "/user/" + id);
		return "users/edit";
	}

	// POST /users/{id} - edit a user
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String edit(@PathVariable("id") long id, @ModelAttribute RegisterFormData rform, Model model) {
		formDataRepository.save(rform);
		return "redirect:/admin/" + id;
	}
	
	
	

}
